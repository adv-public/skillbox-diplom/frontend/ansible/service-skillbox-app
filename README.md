# Docker role `service deploy`
Generic role for creating systemd services to manage docker containers.

Service deploy with Ansible:

```
cd /ansible
ansible-playbook playbooks/pl_service_golang.yml
```

## Example

Example of a Systemd unit for your app:

```yaml
## Deploy service golang with Ansible
---
- name: Docker Service deploy
  hosts: all
  become: true
  gather_facts: true

  vars:
    # image for skillbox/app
    env_app_image: skillbox/app
    project_path: /var/skillbox_app

  tasks:
      - name: Docker container prepare
        include_role: 
          name: docker_file_prepare

      - name: Service deploy
        include_role: 
          name: service_deploy
        vars:
          container_name: app
          container_image: "{{env_app_image}}"
          service_state: started
```

This will create:


* A systemd unit which starts and stops the container. The unit will be called `<name>_container.service` to avoid name clashes.
